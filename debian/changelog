flask-autoindex (0.6.6-4) UNRELEASED; urgency=medium

  * Team upload.

  [ Alexandre Detiste
  * snapshot of upstream code that removes usage of future
    Closes: #1058997

 -- Andreas Tille <tille@debian.org>  Fri, 09 Feb 2024 17:54:58 +0100

flask-autoindex (0.6.6-3) unstable; urgency=medium

  * Team upload.
  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.

  [ Carsten Schoenert ]
  * d/gbp.conf: Add default config
  * Adding a patch queue
    Added patch:
    tests-Ignore-test_own_page-partially.patch
    This patch is dropping some parts of the test test_own_page(), so no
    real fix of the underlying issue that has no effect to the package
    itself.
    (Closes: #1026614)
  * d/watch: Update to version 4 and git mode on GH
  * d/control: Add BuildProfileSpecs to B-D
  * d/control: Bump Standards-Version to 4.6.2
    No further modifications needed.
  * d/control: Update upstream homepage data
  * d/copyright: Some small updates to upstream data
  * d/u/metadata: Adding project metadata
  * python-flask-autoindex-doc: Use dh_sphinxdoc for build
  * d/README.source: Adding information about source
  * d/control: Mark python-flask-autoindex-doc M-A: foreign
  * debian/*: Running wrap-and-sort -ast
    No further modifications.
  * d/control: Sort binary packages alphabetically

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 01 Jan 2023 12:34:34 +0100

flask-autoindex (0.6.6-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Jonathan Carter ]
  * Update debhelper-compat to level 13
  * Install usr/bin/*
  * Fix doc-base paths

 -- Jonathan Carter <jcc@debian.org>  Tue, 03 Nov 2020 11:58:43 +0200

flask-autoindex (0.6.6-1) unstable; urgency=medium

  * New upstream release
    - Upstream licence change, bsd -> mit/expat
    - Packaging license change, bsd -> mit/expat
  * Update standards version to 4.5.0
  * Remove documentation privacy patch that is no longer required

 -- Jonathan Carter <jcc@debian.org>  Sat, 28 Mar 2020 11:16:57 +0200

flask-autoindex (0.6.2-5) unstable; urgency=medium

  * Team upload.
  * Drop python2 support; Closes: #936523

 -- Sandro Tosi <morph@debian.org>  Wed, 27 Nov 2019 21:04:59 -0500

flask-autoindex (0.6.2-4) unstable; urgency=medium

  * Fix docbase path
  * Update standards version to 4.4.1
  * Remove ancient versioned dependency on python-flask

 -- Jonathan Carter <jcc@debian.org>  Wed, 16 Oct 2019 10:17:58 +0200

flask-autoindex (0.6.2-3) unstable; urgency=medium

  * Revert python2 drop from previous upload
    - Due to reverse dependency on python-sagenb

 -- Jonathan Carter <jcc@debian.org>  Thu, 19 Sep 2019 13:52:51 +0000

flask-autoindex (0.6.2-2) unstable; urgency=medium

  * Drop python2 support (Closes: #936523)
  * Update standards version to 4.4.0
  * Upgrade to debhelper-compat (=12)
  * Declare Root-Requires-Root: no
  * Update copyright year

 -- Jonathan Carter <jcc@debian.org>  Thu, 19 Sep 2019 13:18:24 +0000

flask-autoindex (0.6.2-1) unstable; urgency=medium

  * New upstream release
  * Update standards version to 4.2.1

 -- Jonathan Carter <jcc@debian.org>  Mon, 10 Sep 2018 10:22:53 +0200

flask-autoindex (0.6.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field

  [ Jonathan Carter ]
  * New upstream release
  * Update standards version to 4.2.0

 -- Jonathan Carter <jcc@debian.org>  Thu, 02 Aug 2018 17:38:39 +0200

flask-autoindex (0.6+0git20160725-02b454-6) unstable; urgency=medium

  * Re-introduce python2 package (Closes: #893172)

 -- Jonathan Carter <jcc@debian.org>  Mon, 19 Mar 2018 14:05:35 +0200

flask-autoindex (0.6+0git20160725-02b454-5) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Jonathan Carter ]
  * Update compat to level 11
  * Update standards version to 4.1.3
  * Drop python2 version

 -- Jonathan Carter <jcc@debian.org>  Mon, 12 Mar 2018 10:55:13 +0200

flask-autoindex (0.6+0git20160725-02b454-3) unstable; urgency=medium

  * Fix python3 symlinks (Closes: #881274)

 -- Jonathan Carter <jcc@debian.org>  Mon, 13 Nov 2017 14:38:28 +0200

flask-autoindex (0.6+0git20160725-02b454-2) unstable; urgency=medium

  * Fix typo in debian/copyright
  * Add python3-sphinx as build-dependency
  * Update standards version to 4.1.1 (no related changes)

 -- Jonathan Carter <jcc@debian.org>  Tue, 17 Oct 2017 15:58:44 +0200

flask-autoindex (0.6+0git20160725-02b454-1) unstable; urgency=medium

  * Use correct flask_silk imports (upstream fix in git)

 -- Jonathan Carter <jcc@debian.org>  Tue, 05 Sep 2017 11:48:30 +0200

flask-autoindex (0.6-2) unstable; urgency=medium

  * Adopting packages as part of Python Team (DPMT) (Closes: #873648)
    - Set new maintainer/uploaders
    - Update debian/copyright
  * Bump compat level to 10
  * Update standards version to 4.1.0.0
  * Set testsuite: autopkgtest-pkg-python
  * Add install file for python3 version

 -- Jonathan Carter <jcc@debian.org>  Mon, 04 Sep 2017 12:25:44 +0200

flask-autoindex (0.6-1) unstable; urgency=medium

  * New upstream release.
  * Build Python 3 package
  * debian/patches:
    - Remove patches integrated upstream.
    - use-default-theme.patch: Use the default sphinx theme since
      flask-small is no longer included.
  * debian/rules: Switch to pybuild.
  * debian/control:
    - Update Vcs-Git.
    - Add python-future to Build-Depensd.
    - Add Python 3 versions of the Build-Depends.
  * debian/copyright:
    - Remove unused paragraph.
    - Update copyright years.

 -- Sebastian Ramacher <sramacher@debian.org>  Sat, 30 Jan 2016 18:27:40 +0100

flask-autoindex (0.5-4) unstable; urgency=medium

  * debian/control:
    - Move package to collab-maint.
    - Bump Standards-Version.
    - Add dh-python to Build-Depends.
  * debian/{rules,python-flask-babel.pyremove}: Use dh_python2 to remove
    SOURCES.txt. (Closes: #803263)
  * debian/python-flask.babel.{pyremove,links}: Move HTML, CSS and image files
    to /usr/share.
  * debian/watch: Use PyPI redirector.
  * debian/patches/github-fork.patch: Copy from flask-openid to fix
    privacy-breach-generic from lintian.

 -- Sebastian Ramacher <sramacher@debian.org>  Wed, 28 Oct 2015 21:23:07 +0100

flask-autoindex (0.5-3) unstable; urgency=low

  * debian/patches/flask-0.10.patch: Upstream patch to fix test suite with
    flask 0.10. (Closes: #719302)

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 11 Aug 2013 13:35:05 +0200

flask-autoindex (0.5-2) unstable; urgency=low

  * Upload to unstable.

 -- Sebastian Ramacher <sramacher@debian.org>  Thu, 09 May 2013 15:01:32 +0200

flask-autoindex (0.5-1) experimental; urgency=low

  * Initial release. (Closes: #703531)

 -- Sebastian Ramacher <sramacher@debian.org>  Wed, 17 Apr 2013 14:34:53 +0200
